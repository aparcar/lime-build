From bd180a6c234b2becdd7644254ebcae162b074fa6 Mon Sep 17 00:00:00 2001
From: John Crispin <john@phrozen.org>
Date: Wed, 5 Jun 2019 16:03:51 +0200
Subject: [PATCH 18/39] backport: toolchain: add support for custom toolchains

The requirement for being able to add custom src toolchains to the build
system has been brought forward by the members of the prpl foundation.
This patch tries to address this requirement by allowing a ned folder to be
loaded into the tree call toolchain_custom. The subfolders contained within
have the same layout as the toolchain folder. By placing optional Makefiles
into these subfolders It is possible to override the versions of the various
toolchain components aswell as their patch sets and make templates.

Signed-off-by: John Crispin <john@phrozen.org>
---
 .gitignore                         | 1 +
 rules.mk                           | 5 +++++
 scripts/config/zconf.lex.c_shipped | 2 +-
 toolchain/Config.in                | 5 +++++
 toolchain/Makefile                 | 2 ++
 toolchain/binutils/Makefile        | 4 ++++
 toolchain/gcc/common.mk            | 4 ++++
 toolchain/gcc/initial/Makefile     | 4 ++++
 toolchain/gcc/minimal/Makefile     | 4 ++++
 toolchain/gdb/Makefile             | 4 ++++
 toolchain/musl/Makefile            | 2 ++
 toolchain/musl/common.mk           | 2 ++
 12 files changed, 38 insertions(+), 1 deletion(-)

diff --git a/.gitignore b/.gitignore
index 6549af83be..939967833f 100644
--- a/.gitignore
+++ b/.gitignore
@@ -17,6 +17,7 @@
 /package/feeds
 /package/openwrt-packages
 /*.patch
+/toolchain_custom
 key-build*
 *.orig
 *.rej
diff --git a/rules.mk b/rules.mk
index 80cb3d63f4..7596250388 100644
--- a/rules.mk
+++ b/rules.mk
@@ -119,8 +119,13 @@ INCLUDE_DIR:=$(TOPDIR)/include
 SCRIPT_DIR:=$(TOPDIR)/scripts
 BUILD_DIR_BASE:=$(TOPDIR)/build_dir
 ifeq ($(CONFIG_EXTERNAL_TOOLCHAIN),)
+ ifeq ($(CONFIG_CUSTOM_TOOLCHAIN),)
   GCCV:=$(call qstrip,$(CONFIG_GCC_VERSION))
   LIBC:=$(call qstrip,$(CONFIG_LIBC))
+ else
+  GCCV:=$(call qstrip,$(CONFIG_CUSTOM_GCC_VERSION))
+  LIBC:=$(call qstrip,$(CONFIG_CUSTOM_LIBC))
+ endif
   REAL_GNU_TARGET_NAME=$(OPTIMIZE_FOR_CPU)-openwrt-linux$(if $(TARGET_SUFFIX),-$(TARGET_SUFFIX))
   GNU_TARGET_NAME=$(OPTIMIZE_FOR_CPU)-openwrt-linux
   DIR_SUFFIX:=_$(LIBC)$(if $(CONFIG_arm),_eabi)
diff --git a/scripts/config/zconf.lex.c_shipped b/scripts/config/zconf.lex.c_shipped
index 1ca209f511..4a5be3389c 100644
--- a/scripts/config/zconf.lex.c_shipped
+++ b/scripts/config/zconf.lex.c_shipped
@@ -2464,7 +2464,7 @@ void zconf_nextfile(const char *name)
 	err = glob(name, GLOB_ERR | GLOB_MARK, NULL, &gl);
 
 	/* ignore wildcard patterns that return no result */
-	if (err == GLOB_NOMATCH && strchr(name, '*')) {
+	if (((err == GLOB_NOMATCH) || (err == GLOB_ABORTED)) && strchr(name, '*')) {
 		err = 0;
 		gl.gl_pathc = 0;
 	}
diff --git a/toolchain/Config.in b/toolchain/Config.in
index 95087b7078..67f4c30040 100644
--- a/toolchain/Config.in
+++ b/toolchain/Config.in
@@ -155,6 +155,11 @@ menuconfig EXTERNAL_TOOLCHAIN
 		  Specify additional directories searched for libraries (override LDFLAGS).
 		  Use ./DIR for directories relative to the root above.
 
+config CUSTOM_TOOLCHAIN
+	depends on DEVEL
+
+source "toolchain_custom/*.in"
+
 config NEED_TOOLCHAIN
 	bool
 	depends on DEVEL
diff --git a/toolchain/Makefile b/toolchain/Makefile
index 23a5529eed..f1826ed6c2 100644
--- a/toolchain/Makefile
+++ b/toolchain/Makefile
@@ -93,6 +93,8 @@ endif
 
 $(curdir)/install: $(curdir)/compile
 
+include $(wildcard toolchain_custom/*.mk)
+
 $(eval $(call stampfile,$(curdir),toolchain,compile,$(TOOLCHAIN_DIR)/stamp/.gcc-initial_installed,,$(TOOLCHAIN_DIR)))
 $(eval $(call stampfile,$(curdir),toolchain,check,$(TMP_DIR)/.build))
 $(eval $(call subdir,$(curdir)))
diff --git a/toolchain/binutils/Makefile b/toolchain/binutils/Makefile
index 24eaf70566..04620a8769 100644
--- a/toolchain/binutils/Makefile
+++ b/toolchain/binutils/Makefile
@@ -31,6 +31,8 @@ HOST_BUILD_PARALLEL:=1
 
 PATCH_DIR:=./patches/$(PKG_VERSION)
 
+include $(wildcard $(TOPDIR)/toolchain_custom/binutils/*.var)
+
 include $(INCLUDE_DIR)/toolchain-build.mk
 
 HOST_CONFIGURE_ARGS = \
@@ -99,4 +101,6 @@ define Host/Clean
 		$(BUILD_DIR_TOOLCHAIN)/$(PKG_NAME)
 endef
 
+include $(wildcard $(TOPDIR)/toolchain_custom/binutils/*.build)
+
 $(eval $(call HostBuild))
diff --git a/toolchain/gcc/common.mk b/toolchain/gcc/common.mk
index e2dfcea6cb..9ba48cdbc5 100644
--- a/toolchain/gcc/common.mk
+++ b/toolchain/gcc/common.mk
@@ -47,6 +47,8 @@ PKGVERSION=OpenWrt GCC $(PKG_VERSION) $(REVISION)
 
 HOST_BUILD_PARALLEL:=1
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gcc/*.var)
+
 include $(INCLUDE_DIR)/toolchain-build.mk
 
 HOST_SOURCE_DIR:=$(HOST_BUILD_DIR)
@@ -229,3 +231,5 @@ define Host/Clean
 		$(TOOLCHAIN_DIR)/bin/$(REAL_GNU_TARGET_NAME)-gc* \
 		$(TOOLCHAIN_DIR)/bin/$(REAL_GNU_TARGET_NAME)-c*
 endef
+
+include $(wildcard $(TOPDIR)/toolchain_custom/gcc/*.build)
diff --git a/toolchain/gcc/initial/Makefile b/toolchain/gcc/initial/Makefile
index c71b17dd87..b9ada19ec1 100644
--- a/toolchain/gcc/initial/Makefile
+++ b/toolchain/gcc/initial/Makefile
@@ -1,6 +1,8 @@
 GCC_VARIANT:=initial
 GCC_PREPARE=$(CONFIG_USE_MUSL)
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gcc/initial/*.var)
+
 include ../common.mk
 
 GCC_CONFIGURE += \
@@ -33,4 +35,6 @@ define Host/Install
 	$$(call file_copy,$(TOOLCHAIN_DIR)/initial/.,$(TOOLCHAIN_DIR)/)
 endef
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gcc/initial/*.build)
+
 $(eval $(call HostBuild))
diff --git a/toolchain/gcc/minimal/Makefile b/toolchain/gcc/minimal/Makefile
index 31d6f675ab..0b5c485dd8 100644
--- a/toolchain/gcc/minimal/Makefile
+++ b/toolchain/gcc/minimal/Makefile
@@ -1,6 +1,8 @@
 GCC_VARIANT:=minimal
 GCC_PREPARE=$(if $(CONFIG_USE_MUSL),,1)
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gcc/minimal/*.var)
+
 include ../common.mk
 
 GCC_CONFIGURE += \
@@ -26,4 +28,6 @@ define Host/Clean
 		$(GCC_BUILD_DIR)
 endef
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gcc/minimal/*.build)
+
 $(eval $(call HostBuild))
diff --git a/toolchain/gdb/Makefile b/toolchain/gdb/Makefile
index 41ba9853fd..7aedef8a5c 100644
--- a/toolchain/gdb/Makefile
+++ b/toolchain/gdb/Makefile
@@ -29,6 +29,8 @@ HOST_BUILD_DIR:=$(BUILD_DIR_TOOLCHAIN)/$(GDB_DIR)
 
 HOST_BUILD_PARALLEL:=1
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gdb/*.var)
+
 include $(INCLUDE_DIR)/toolchain-build.mk
 
 HOST_CONFIGURE_VARS += \
@@ -65,4 +67,6 @@ define Host/Clean
 		$(TOOLCHAIN_DIR)/bin/$(GNU_TARGET_NAME)-gdb
 endef
 
+include $(wildcard $(TOPDIR)/toolchain_custom/gdb/*.build)
+
 $(eval $(call HostBuild))
diff --git a/toolchain/musl/Makefile b/toolchain/musl/Makefile
index 2b9312bcbf..aec678ebbe 100644
--- a/toolchain/musl/Makefile
+++ b/toolchain/musl/Makefile
@@ -28,4 +28,6 @@ define Host/Install
 	$(CP) ./include $(TOOLCHAIN_DIR)/
 endef
 
+include $(wildcard $(TOPDIR)/toolchain_custom/musl/*.build)
+
 $(eval $(call HostBuild))
diff --git a/toolchain/musl/common.mk b/toolchain/musl/common.mk
index bb9d9a75a5..e6de125ce0 100644
--- a/toolchain/musl/common.mk
+++ b/toolchain/musl/common.mk
@@ -25,6 +25,8 @@ BUILD_DIR_HOST:=$(BUILD_DIR_TOOLCHAIN)
 HOST_BUILD_PREFIX:=$(TOOLCHAIN_DIR)
 HOST_BUILD_DIR:=$(BUILD_DIR_TOOLCHAIN)/$(PKG_NAME)-$(PKG_VERSION)
 
+include $(wildcard $(TOPDIR)/toolchain_custom/musl/*.var)
+
 include $(INCLUDE_DIR)/host-build.mk
 include $(INCLUDE_DIR)/hardening.mk
 
-- 
2.20.1

