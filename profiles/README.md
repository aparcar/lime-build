# prplProfiles

Profiles allow a modular configuration for images. It is possible to change
every aspect of the build system in via reusable `yaml` configuration files.

A profile can contain a specific device but also a configuration for specific
features like a container runtime or a webinterface.

## Configuration

This section describes all possible configuration options understood by the
`gen_config.py` script.

### `profile`

The OpenWrt device profile to be build. This value can only exists once in an
profile combination.

Example:
```yaml
profile: linksys_wrt3200acm
```

### `target` and `subtarget`

Architecture for the build images.

Example:
```yaml
target: mvebu
subtarget: cortexa9
```

### `description`

A verbose description that describes the functionality of a profile

Example:
```yaml
description: Build image for the Linksys WRT3200ACM
```

### `external_target`

Using a target that is not part of the OpenWrt repository needs to be actively
installed. This is done by activating this option.

Example:
```yaml
target: intel_mips
subtarget: xrx500
external_target: True
```

### `feeds`

An array of feeds to be installed in addition to the default feeds. Each feed
must contain the values `name` and `uri` while the options `branch` and
`revision` are optional but exclusive.

Example:
```yaml
feeds:
  - name: feed_target_mips
    uri: https://intel.prpl.dev/intel/feed_target_mips.git
    branch: ugw-8.4.1-cleanup
  - name: feed_datapath
    uri: https://intel.prpl.dev/intel/feed_datapath.git
    revision: 00ccbdf6
```

### `packages`

An optional list of packages that should be installed by the profile.

Example:
```yaml
packages:
  - dwpal_6x-uci
  - iwlwav-base-files
```

## `diffconfig`

Additional options added directly to the `.config`.

```yaml
diffconfig: |
  CONFIG_DEVEL=y
  CONFIG_KERNEL_GIT_CLONE_URI="https://intel.prpl.dev/intel/linux.git"
  CONFIG_KERNEL_GIT_DEPTH="1000"
  CONFIG_KERNEL_GIT_REF="727acdb060410a936330a2456f641b9473bf5121"
```
