from pathlib import Path
from subprocess import run
from time import sleep
import os
import sys
import yaml

import gen_config

if len(sys.argv) == 1:
    if not Path("config.yml").is_file():
        print("Missing config.yml")
        sys.exit(1)

    config = yaml.safe_load(open("config.yml"))
elif len(sys.argv) > 4:
    repo = sys.argv[1]
    branch = sys.argv[2]
    build_id = sys.argv[3]
    profiles = sys.argv[4:]
    config = {
        "repo": repo,
        "branch": branch,
        "builds": {build_id: {"profiles": profiles}},
    }
else:
    print(
        f"""Usage: {sys.argv[0]} [config]|[options...]
    The prplbuild script supports three different modes:

    {sys.argv[0]}:
        Load configuration called config.yml from current directory
    {sys.argv[0]} <repo> <branch> <build_id> <profile> [profile...]:
        Generate configuration based on passed arguments. Any number of
        profiles can be attached after the first profile
    """
    )
    print("usage")
    sys.exit(1)

base_dir = Path.cwd().absolute()

output_dir = Path(config["output_dir"]).absolute()
print(f"Output directory is {output_dir}")

patches = []
for folder in config.get("patch_folders", []):
    patch_folder = base_dir / folder
    if not patch_folder.is_dir():
        gen_config.die(f"Patch folder {patch_folder} not found")

    print(f"Adding patches from {patch_folder}")

    patches.extend(
        sorted(list((base_dir / folder).glob("*.patch")), key=os.path.basename)
    )

print(f"Found {len(patches)} patches")


if not Path("openwrt/Makefile").is_file():
    print("Clone openwrt.git")
    Path("openwrt").mkdir(exist_ok=True, parents=True)
    run(["git", "clone", config["repo"], "."], cwd="openwrt", check=True)

while True:
    for build_id, data in config["builds"].items():
        build_output_dir = output_dir / build_id
        build_output_dir.mkdir(exist_ok=True, parents=True)

        os.chdir("openwrt")
        try:
            print(f"Building {build_id}")
            gen_config.clean_tree()

            run(
                ["git", "config", "user.email", config["git_email"]], check=True,
            )
            run(
                ["git", "config", "user.name", config["git_name"]], check=True,
            )
            run(
                ["git", "fetch", config["repo"], data.get("branch", config["branch"])],
                check=True,
            )
            run(
                ["git", "checkout", data.get("branch", config["branch"])], check=True,
            )
            run(["git", "clean", "-d", "-f"], check=True)
            run(
                ["git", "reset", "--hard", config.get("revision", config["branch"])],
                check=True,
            )

            for patch in patches:
                print(f"Applying patch {patch}")
                run(["git", "am", "-3", str(base_dir / patch)], check=True)

            profile = gen_config.merge_profiles(data["profiles"])
            gen_config.setup_feeds(profile)
            gen_config.generate_config(profile)

            if config.get("filename_include_build_id", True):
                extra_image_name = build_id
            else:
                extra_image_name = ""

            run(["make", "defconfig"], check=True)
            run(["make", "clean"], check=True)

            run(
                [
                    "make",
                    "-j",
                    str(os.cpu_count()),
                    f"BIN_DIR={build_output_dir}",
                    "BUILD_LOG=y",
                    f"BUILD_LOG_DIR={build_output_dir}/logs",
                    f"EXTRA_IMAGE_NAME={extra_image_name}",
                ],
                check=True,
            )
            if (build_output_dir / "FAIL").is_file():
                (build_output_dir / "FAIL").unlink()
        except:
            print(f"Building {build_id} failed")
            (build_output_dir / "FAIL").write_text("stamp")

        finally:
            os.chdir(base_dir)
    if config.get("interval", 0) > 0:
        print(f"Next build in {config['interval']} hours")
        sleep(60 * 60 * int(config["interval"]))
    else:
        print("All done")
        break
