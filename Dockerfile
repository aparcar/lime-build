FROM openwrtorg/sdk

RUN sudo apt-get update -qq &&\
    sudo apt-get install -y \
        python3-yaml \
        && sudo apt-get -y autoremove \
        && sudo apt-get clean \
        && sudo rm -rf /var/lib/apt/lists/*

WORKDIR /home/build/
RUN rm -rf openwrt/
ADD prplbuild.py .
ADD gen_config.py .
ADD ./patches/ ./patches

ENTRYPOINT ["python3", "prplbuild.py"]
