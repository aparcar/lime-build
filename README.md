# prplBuild

This script builds prplWrt based on profiles. It can run directly on the
developer machine, within a Docker container or on a server as a continues
service.

It clones `openwrt.git` or any given clone and builds defined profiles based on
a given `config.yml`. Every iteration cleans up builds environment to provide
clean builds.

All patches from the `patches/` folder are applied in ascending filename order,
the folder structure does not influence the order.

## `config.yml`

Below is an illustrative `config.yml` file building two images:

```yaml
interval: 24 # in hours

repo: https://github.com/openwrt/openwrt.git
branch: openwrt-19.07
revision: 83b714a2

output_dir: ./output
filename_include_build_id: True

# used for applying patches
git_email: "prplbuild@prpl.dev"
git_name: "prplBuild"

builds:
  wrt32x-minimal:
    profiles:
      - wrt32x
      - ccache
  wrt32x-webui:
    profiles:
      - wrt32x
      - webui
      - uxc
      - ccache
```

The *build id* determines the subfolder for created files and should therefore
only contain supported characters.

## Docker

Instead of installing dependencies it is also possible to use Docker. The
`Dockerfile` is based on the `openwrtorg/sdk` container and copies all required
files. When using the Docker container one must use *volumes* to access files
within the container.

* `/home/build/output` output path for stored files.
* `/home/build/config.yml` read configuration file
* `/home/build/openwrt` git repository (optional)

A possible command to use the docker container could look like the following:

```bash
# run.sh
docker build -t prplbuild:latest .
docker run -it \
	-v "$(pwd)/config.yml:/home/build/config.yml" \
	-v "$(pwd)/output/:/home/build/output/" \
	prplbuild:latest
```

When using Docker the `output/` folder needs adequate permission so the `build`
user withn the Docker container can write. The easiest is to give the `output/`
folder the permission `777` and the top folder `600`. This way the Docker user
can write while other users on the host system cannot write into the `output/`
folder.

### `docker-compose.yml`

It is also possible to run the Docker container automatically via
`docker-compose`. This brings the advantage of an running `nginx` server which
automatically hosts the `output/` directory.
